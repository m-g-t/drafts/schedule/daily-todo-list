import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, compose } from 'redux';
import root from './modules/root';

import App from './App';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const store = createStore(root, (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose)());

ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.querySelector('#root'));
