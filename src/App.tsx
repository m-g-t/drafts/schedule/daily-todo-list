import React from 'react';

import ItemList from './containers/ItemList';
import AddForm from './containers/AddForm';

import './App.scss';

function App(): JSX.Element {
  return (
    <ul className="_">
      <ItemList />
      <AddForm />
    </ul>
  );
}

export default App;
