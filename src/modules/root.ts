import { combineReducers } from 'redux';
import todolist from './todolist';

const root = combineReducers({
  todolist,
});

export default root;

export type RootState = ReturnType<typeof root>;
