import {
  createAction,
  ActionType,
  createReducer,
} from 'typesafe-actions';

import { Item } from '../common';

const ADD = 'todolist/ADD';
const MODIFY = 'todolist/MODIFY';
const CHECK = 'todolist/CHECK';

export const add = createAction(ADD)<string>();
export const modify = createAction(MODIFY)<[number, string]>();
export const check = createAction(CHECK)<number>();

const actions = { add, modify, check };
type TodoListAction = ActionType<typeof actions>;

type TodoListState = {
  items: Item[];
}

const initialState: TodoListState = {
  items: [],
};

const todolist = createReducer<TodoListState, TodoListAction>(initialState, {
  [ADD]: ({ items }, { payload: name }) => ({ items: [...items, [name, false]] }),
  [MODIFY]: (state, action) => ({ items: [] }),
  [CHECK]: (state, { payload: index }) => {
    const { items: old } = state;
    if (old[index]) {
      const items = [...old];
      items[index] = [items[index][0], !items[index][1]];
      return { items };
    }
    return state;
  },
});

export default todolist;
