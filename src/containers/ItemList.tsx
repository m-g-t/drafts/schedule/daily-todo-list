import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../modules/root';
import { check } from '../modules/todolist';
import Presentational from '../components/ItemList';

function ItemList(): JSX.Element {
  const items = useSelector((state: RootState) => state.todolist.items);
  const dispatch = useDispatch();

  const onChange = (index: number): void => {
    dispatch(check(index));
  };

  return (
    <Presentational
      items={items}
      onChange={onChange}
    />
  );
}

export default ItemList;
