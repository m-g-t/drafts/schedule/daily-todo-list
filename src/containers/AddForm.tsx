import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../modules/root';
import { add } from '../modules/todolist';
import Presentational from '../components/AddForm';

function AddForm(): JSX.Element {
  const items = useSelector((state: RootState) => state.todolist.items);
  const dispatch = useDispatch();

  const addItem = (name: string): void => {
    dispatch(add(name));
  };

  return (
    <Presentational
      items={items}
      addItem={addItem}
    />
  );
}

export default AddForm;
