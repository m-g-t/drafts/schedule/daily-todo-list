import React from 'react';

import { Item } from '../common';

interface Props {
  items: Item[];
  addItem: (value: string) => void;
}

interface State {
  value: string;
}

class AddForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { value: '' };
    this.addItem = this.addItem.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  shouldComponentUpdate(nextProps: Props, nextState: State): boolean {
    const { value } = this.state;
    return value !== nextState.value;
  }

  handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (event.key === 'Enter') {
      this.handleClick();
    }
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ value: event.target.value });
  }

  handleClick(): void {
    this.setState(this.addItem);
  }

  addItem(): State {
    const { addItem } = this.props;
    const { value } = this.state;
    addItem(value);
    return { value: '' };
  }

  render(): React.ReactElement {
    const { value } = this.state;
    return (
      <li>
        <button type="button" onClick={this.handleClick}>+</button>
        <input type="text" value={value} onChange={this.handleChange} onKeyDown={this.handleKeyDown} />
      </li>
    );
  }
}

export default AddForm;
