import React from 'react';

import { Item } from '../common';
import './TodoItem.scss';

interface Props {
  item: Item;
  index: number;
  onChange: (index: number) => void;
}

class TodoItem extends React.Component<Props> {
  shouldComponentUpdate(nextProps: Props): boolean {
    const { item, index } = this.props;
    return item !== nextProps.item || index !== nextProps.index;
  }

  render(): JSX.Element {
    const { item, index, onChange } = this.props;
    return (
      <li>
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <label>
          <input type="checkbox" checked={item[1]} onChange={(): void => onChange(index)} />
          {item[0]}
        </label>
      </li>
    );
  }
}

export default TodoItem;
