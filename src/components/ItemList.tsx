import React from 'react';

import { Item } from '../common';
import TodoItem from './TodoItem';

interface Props {
  items: Item[];
  onChange: (index: number) => void;
}

function ItemList({ items, onChange }: Props): JSX.Element {
  return (
    <>
      {
        items.map((item, index) => (
          <TodoItem item={item} index={index} onChange={onChange} key={index.toString()} />
        ))
      }
    </>
  );
}

export default ItemList;
