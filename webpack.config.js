/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');

module.exports = {
  entry: './src/index.tsx',
  output: {
    filename: 'index.js',
    path: path.resolve(path.join(__dirname, 'build')),
  },
  mode: 'development',
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve('./build'),
    index: 'index.html',
    port: 49101,
  },
  resolve: { extensions: ['.wasm', '.mjs', '.js', '.jsx', '.ts', '.tsx', '.json'] },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ['ts-loader'],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: 'index.html',
    }),
    new MiniCssExtractPlugin({
      filename: 'style.css',
    }),
    new CleanWebpackPlugin(),
    new WebpackPwaManifest({
      filename: 'manifest.json',
      name: 'Daily TODO list',
      short_name: 'TODOLIST', // eslint-disable-line @typescript-eslint/camelcase
      description: 'm-g-t/drafts/schedule/daily-todo-list',
      background_color: '#123456', // eslint-disable-line @typescript-eslint/camelcase
      crossorigin: 'use-credentials',
      fingerprints: false,
      icons: [
        {
          src: path.resolve('src/images/icon.png'),
          sizes: [96, 128, 192, 256, 384, 512],
        },
      ],
    }),
  ],
};
