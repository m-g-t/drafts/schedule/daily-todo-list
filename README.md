# Daily TODO List

Daily TODO List - PWA

## Getting Started

### Prerequisites

Node.js, browser

### Build

get source

```bash
git clone https://gitlab.com/m-g-t/drafts/schedule/daily-todo-list.git
```

and install dependencies

```bash
npm ci
```

test

```bash
npm start
```

You can see the results at [http://localhost:49101/](http://localhost:49101/).

```bash
npm run build
```

the output files are in the `build` folder

## License

UNLICENSED
